/*
 *  Copyright (c) 2017. Selwyn Dijkstra [se.dijkstra@st.hanze.nl] & Lin Chang [l.z.chang@st.hanze.nl].
 *  All rights reserved.
 */

package nl.bioinf.selwynlin.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;


@WebServlet(name = "StringParser", urlPatterns = "/action_page")
public class StringParser extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("molecule");
        PythonCaller P = new PythonCaller();
        String text = P.PythonRun(name);

        // Prints text realtime to .jsp page
        PrintWriter out = response.getWriter();
        response.setContentType("text/plain;charset=UTF-8");
        out.println(text);
    }

    /**
     * This class is used to run a python script and receive its output back.
     * In this case the output is a JSON object which is used to feed SigmaJS.
     */
    public class PythonCaller {

        /**
         * Runs the grappa python script with runtime.exec to receive the output of the script
         * @param args the grappa string to create
         * @return the output of the script
         * @throws IOException
         */
        public String PythonRun(String args) throws IOException {
            // Runs a python script and returns the print values
            String pythonScriptPath = "/commons/student/2017-2018/Thema10/fappa/grappa.py";
            String[] cmd = new String[3];
            cmd[0] = "python3";
            cmd[1] = pythonScriptPath;
            cmd[2] = args;
            // create runtime to execute external command
            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec(cmd);

            StringBuilder sb = new StringBuilder();

            // retrieve output from python script
            BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            BufferedReader error = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
            String line = "";
            while ((line = bfr.readLine()) != null) {
                sb.append(line);
            }
            System.out.println(sb);
            return sb.toString();

        }
    }

}
