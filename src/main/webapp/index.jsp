<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sdijkstra
  Date: 4-12-17
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="css/visualizerPage.css">
    <link rel="stylesheet" type="text/css" href="css/sticky-footer.css">

    <title>Molecule Graph Visualizer</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-light">
    <a class="navbar-brand">Molecule structure visualizer</a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<c:url value="/index.jsp"/>">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="help" href="#help">Help</a>
        </li>
    </ul>
</nav>

<div class="home">
    <div class="container-fluid">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" name="drop" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Select a molecule
            </button>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" id="<LYS>">Lysine</a>
                <a class="dropdown-item" id="<GLY>">Glycine</a>
                <a class="dropdown-item" id="<ALA>">Alanine</a>
                <a class="dropdown-item" id="<ASP>">Aspartate</a>
                <a class="dropdown-item" id="<ASN>">Asparagine</a>
                <a class="dropdown-item" id="<SER>">Serine</a>
                <a class="dropdown-item" id="<CYS>">Cysteine</a>
                <a class="dropdown-item" id="<MET>">Methionine</a>
                <a class="dropdown-item" id="<THR>">Threonine</a>
                <a class="dropdown-item" id="<VAL>">Valine</a>
                <a class="dropdown-item" id="<ILE>">Isoleucine</a>
                <a class="dropdown-item" id="<LEU>">Leucine</a>
                <a class="dropdown-item" id="<GLU>">Glutamate</a>
                <a class="dropdown-item" id="<GLN>">Glutamine</a>
                <a class="dropdown-item" id="<PRO>">Proline</a>
                <a class="dropdown-item" id="<HIS>">Histidine</a>
                <a class="dropdown-item" id="<PHE>">Phenylalanine</a>
                <a class="dropdown-item" id="<TYR>">Tyrosine</a>
                <a class="dropdown-item" id="<ARG>">Arginine</a>
                <a class="dropdown-item" id="<TRP>">Tryptophane</a>
                <a class="dropdown-item" id="<GLUCOSE>">Glucose</a>
            </div>
        </div>
    </div>
    <textarea class="string-text" type="text" name="molecule" id="molecule" rows="4" cols="50"
              placeholder="Please insert your molecule string here."></textarea>
    <div class="table">
        <table id="info_table">
            <tr>
                <th>Node</th>
                <th>Attribute</th>
                <th>Value</th>
            </tr>
        </table>
    </div>

    <div id="graph-container"></div>
</div>

<div class="help">
    <p>The Molecule structure visualizer is a web application for creating molecules and visualizing these in a
        graph.<br/>
        The program accepts a Grappa syntax, which uses the networkX library for creating a graph.

        Attributes can be assigned to the nodes, which will be visible as well.<br/>
        To start building a molecule, enter a valid grappa string in the text box on the homepage.<br/>
        To assist the user, The base block, 20 amino acids and multiple sugars has already been pre-defined in the
        dropdown menu.</p>

    <p>An example of a grapps string is the following:<br/>
        <i>[ BB ] N(H,.) CA(HA,.) C(O1,.) @CA {chiral:(N,C,HA)} </i></p>

    <h3>Grappa string Rules:</h3>
    <p>
        name : add node with name, with edge to active node
        (none at start, active parent at start of branch) <br>
        -name : remove node with name <br>

        @name : select name as active node <br>

        ( : set active node as active parent (start branching) <br>

        , : switch to new branch at active parent <br>

        ) : set active parent to active node <br>

        =nameB : rename active node (keep edges) <br>

        {attr=val} : set attribute on active node (can be
        attributes like: element, charge, valence, stubs
        element is set to FIRST LETTER of name,
        unless specified as attribute
        attribute chiral has tuple of three nodes,
        which define chirality according to right-hand rule <br>

        !X : connect active node to node X, which _must_ be present already
        otherwise, using a name that is already there is an error <br>

        &lt;NAME&gt; : include brick with given name <br>

        <':NAME> : include brick with given name and add ' as suffix to nodes <br>

        &lt;NAME@X&gt; : include brick with given name and add edge between active
        node and node 'X' of brick <br>

        /#=1-20/C#(H#[1-2])/ : Expand by repetion and substitution according to
        range specified <br>

        (/#=A-D/C#(H#[1-3]),/) : Expand to multiple branches. </p>

</div>

<div class="contact">
    <div class="col-md-6">
        <p>We can be contacted at the following addresses</p>
        <p>Selwyn Dijkstra can be reached at se.dijkstra@st.hanze.nl - Student number 344573.</p>
        <p>Lin Chang can be reached at l.z.chang@st.hanze.nl - Student number 345212.</p>
    </div>
</div>

<footer class="footer bg-light">
    <div class="container text-center">
        Made by Lin Chang & Selwyn Dijkstra in cooperation with Tsjerk Wassenaar.
        <a id="contact" href="#contact">Contact</a>
    </div>
</footer>

</body>
<script src="js/lib/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src='js/bootstrap.bundle.js'></script>
<script src='js/bootstrap.js'></script>
<script src='js/realtimeParser.js'></script>
<!-- START SIGMA IMPORTS -->
<script src="js/sigma.min.js"></script>
<script src="js/sigma.require.js"></script>
<!-- END SIGMA IMPORTS -->
<script src="js/plugins/sigma.layout.forceAtlas2.min.js"></script>

</html>
