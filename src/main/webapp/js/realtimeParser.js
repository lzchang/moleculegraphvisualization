$(document).ready(function () {
    var timer = null;
    // When the help button is pressed hides all content except the help menu.
    $("a#help").on('click', function () {
        $(".home").hide();
        $(".help").show();
        $(".contact").hide();

    });
    // When the contact button is pressed hides all content except the contact page.
    $("a#contact").on('click', function () {
        $(".home").hide();
        $(".help").hide();
        $(".contact").show();

    });

    $(".dropdown-item").on('click', function () {
        $("#molecule").val($("#molecule").val() + this.id);
        process();
    });

    // Timeout to create graph when user stops typing for 1,5 seconds
    $("#molecule").on('keyup', function () {
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function () {
            process()
        }, 1500);

    });
});

function process() {
    var xmlhttp = new XMLHttpRequest();
    var notation = document.getElementById("molecule").value;

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var json = JSON.parse(xmlhttp.responseText);

            cleartable();

            for (var i = 0; i < Object.keys(json.nodes).length; i++) {
                var currentNode = json.nodes[i];
                for (var y = 0; y < Object.keys(currentNode).length; y++) {
                    if (Object.keys(currentNode)[y] !== "id" &&
                        Object.keys(currentNode)[y] !== "label" &&
                        Object.keys(currentNode)[y] !== "color" &&
                        Object.keys(currentNode)[y] !== "size") {
                        tableMaker(currentNode.id, Object.keys(currentNode)[y], currentNode[Object.keys(currentNode)[y]]);
                    }
                }
            }

            newgraph(json);
        }
    };

    xmlhttp.open("GET", "/action_page?molecule=" + notation, true);
    xmlhttp.send();
}

function newgraph(jsonString) {
    if (typeof s !== "undefined") {
        s.graph.clear();
        s.refresh();
        $('#graph-container').empty(); //alternative for clearing graph on slower computers
    }

    s = new sigma({
        graph: jsonString,
        container: 'graph-container',
        settings: {
            sideMargin: 1,
            edgeColor: "red",
            labelThreshold: 1,
            minNodeSize: 8,
            maxNodeSize: 22

        }
    });

    s.graph.nodes().forEach(function (node, i) {
        node.x = i;
        node.y = Math.random();
    });

    s.refresh();
    s.startForceAtlas2({slowdown: 5, worker: true});
    setTimeout(function () {
        s.stopForceAtlas2();
    }, 5000);
}

function cleartable() {
    $("#info_table").find("tr>td").remove();
}

function tableMaker(Node, Attr, Val) {
    var table = document.getElementById("info_table");

    var row = table.insertRow(1);

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);

    cell1.innerHTML = Node;  //Node
    cell2.innerHTML = Attr;  //Attribute
    cell3.innerHTML = Val;  //Value
}