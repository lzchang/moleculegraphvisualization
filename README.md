# Molecule Structure Visualizer

The Molecule structure visualizer is a web application for creating molecules and visualizing these in a graph.
The program accepts a Grappa syntax, which uses the networkX library for creating a graph.

Attributes can be assigned to the nodes, which will be visible as well.
To start building a graph, the 20 amino acids are already pre-defined in the application.


## Installation

* Download from source (git clone, zipped package)
* Run index.jsp, located at src\main\webapp\, within Apache Tomcat

## Usage

This is an example of the grappa string method.

    [ BB ] N(H,.) CA(HA,.) C(O1,.) @CA {chiral:(N,C,HA)} 


#### Grappa string Rules:

* name : add node with name, with edge to active node
(none at start, active parent at start of branch)
-name : remove node with name

* \@name : select name as active node 

* ( : set active node as active parent (start branching) 

* , : switch to new branch at active parent 

* ) : set active parent to active node 

* =nameB : rename active node (keep edges) 

* {attr=val} : set attribute on active node (can be
attributes like: element, charge, valence, stubs
element is set to FIRST LETTER of name,
unless specified as attribute
attribute chiral has tuple of three nodes,
which define chirality according to right-hand rule

* !X : connect active node to node X, which must be present already
otherwise, using a name that is already there is an error

* <NAME>; : include brick with given name 

* <':NAME> : include brick with given name and add ' as suffix to nodes

* <NAME@X> : include brick with given name and add edge between active
node and node 'X' of brick

* /#=1-20/C#(H#[1-2])/ : Expand by repetion and substitution according to
range specified

* (/#=A-D/C#(H#[1-3]),/) : Expand to multiple branches.


## Required software

* Apache Tomcat or similar web container
* Grappa 

## FAQ

#### No graph appears when I create a molecule with attributes?

This happens because the web container, Apache Tomcat in this case, does not allow certain characters e.g. the curly brackets which are used for the attributes.

To solve this:

1. Navigate to the root folder of Tomcat.
2. Go to the conf folder.
3. Open Catalina.properties in a text editor
4. Add the following to the catalina.properties file:
tomcat.util.http.parser.HttpParser.requestTargetAllow=|{}
5. Restart Tomcat